use crate::method::Method;
use failure::{Fail, Fallible, _core::cmp::Ordering};
use std::{
    cell::RefCell,
    collections::{hash_map::Entry, BTreeMap, BTreeSet, HashMap},
    fmt,
};
use typed_arena::Arena;

pub struct TypeModuleManager<'a> {
    pool: Arena<TypeModule<'a>>,
    index: RefCell<HashMap<String, &'a TypeModule<'a>>>,
}

#[derive(Debug)]
enum TypeSubmoduleView<'a> {
    Explicit(TypeModuleView<'a>),
    Cyclic
}

#[derive(Debug)]
pub struct TypeModuleView<'a> {
    name: &'a str,
    submodules: Vec<TypeSubmoduleView<'a>>,
    methods: &'a BTreeMap<String, Method>,
}

struct TypeModuleViewGenerator<'a> {
    already_visited_modules: BTreeSet<String>,
    type_module_manager: &'a TypeModuleManager<'a>,
}

impl<'a> TypeModuleViewGenerator<'a> {
    pub fn new(type_module_manager: &'a TypeModuleManager<'a>) -> Self {
        TypeModuleViewGenerator { already_visited_modules: BTreeSet::new(), type_module_manager }
    }

    fn generate(&mut self, module: &'a TypeModule<'a>) -> TypeModuleView<'a> {
        let submodules = module.submodules.iter().map(|module| {
            if self.already_visited_modules.insert(module.name.to_owned()) {
                TypeSubmoduleView::Explicit(self.generate(module))
            } else {
                TypeSubmoduleView::Cyclic
            }
        }).collect();
        TypeModuleView {
            name: &module.name,
            submodules,
            methods: &module.methods,
        }
    }
}


impl fmt::Debug for TypeModuleManager<'_> {
    fn fmt<'a>(&'a self, f: &'a mut fmt::Formatter<'_>) -> fmt::Result {
        let index = self.index.borrow();
        let mut type_modules = index
            .iter()
            .map(|(name, &module)|
                TypeModuleViewGenerator::<'a>::new(self).generate(&module)
            )
            .collect::<Vec<_>>();
        //type_modules.sort_by_key(|module| module.name);

        f.debug_struct("TypeModuleManager").field("type_modules", &type_modules).finish()
    }
}

/*

impl<'a> TypeModuleManager<'a> {
    fn fmt(&'a self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let index = self.index.borrow();
        let mut type_modules = index
            .iter()
            .map(|(name, &module)|
                TypeModuleViewGenerator::new(&self).generate(&module)
            )
            .collect::<Vec<_>>();
        //type_modules.sort_by_key(|module| module.name);

        f.debug_struct("TypeModuleManager").field("type_modules", &type_modules).finish()
    }
}
*/

#[derive(Debug, Fail)]
enum TypeModuleError {
    #[fail(display = "Module `{}` already defined", _0)]
    ModuleAlreadyDefined(String),
    #[fail(display = "Module `{}` is undefined", _0)]
    UndefinedSubmodule(String),
}

impl<'a> TypeModuleManager<'a> {
    pub fn new() -> Self {
        Self {
            pool: Arena::new(),
            index: RefCell::new(HashMap::new()),
        }
    }

    fn add_type_module(&'a self, module: TypeModule<'a>) -> Fallible<()> {
        if let Entry::Vacant(entry) = self.index.borrow_mut().entry(module.name.clone()) {
            let module = self.pool.alloc(module);
            entry.insert(module);
        } else {
            return Err(TypeModuleError::ModuleAlreadyDefined(module.name).into());
        }
        Ok(())
    }

    fn lookup(&self, module_name: &str) -> Fallible<&TypeModule<'a>> {
        Ok(self
            .index
            .borrow()
            .get(module_name)
            .copied()
            .ok_or(TypeModuleError::UndefinedSubmodule(module_name.to_owned()))?)
    }
}

pub struct TypeModuleBuilder {
    name: String,
    submodules: BTreeSet<String>,
    methods: BTreeMap<String, Method>,
}

impl TypeModuleBuilder {
    pub fn new(name: String) -> Self {
        Self {
            name,
            submodules: BTreeSet::new(),
            methods: BTreeMap::new(),
        }
    }

    pub fn add_submodule(mut self, submodule: String) -> Self {
        self.submodules.insert(submodule);
        self
    }

    pub fn add_method(mut self, name: String, method: Method) -> Self {
        self.methods.insert(name, method);
        self
    }

    pub fn build<'a>(self, type_module_manager: &'a TypeModuleManager<'a>) -> Fallible<()> {
        let type_module: TypeModule<'a> = TypeModule {
            name: self.name,
            submodules: self
                .submodules
                .into_iter()
                .map(|module| type_module_manager.lookup(&module))
                .collect::<Fallible<BTreeSet<_>>>()?,
            methods: self.methods,
        };
        type_module_manager.add_type_module(type_module)?;
        Ok(())
    }
}

#[derive(Eq)]
pub struct TypeModule<'a> {
    name: String,
    submodules: BTreeSet<&'a TypeModule<'a>>,
    methods: BTreeMap<String, Method>,
}

impl<'a> PartialOrd for TypeModule<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> Ord for TypeModule<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

impl<'a> PartialEq for TypeModule<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}
