mod method;
mod type_module;

use crate::{method::Method, type_module::TypeModuleBuilder};
use type_module::TypeModuleManager;

fn main() {
    let type_modules = TypeModuleManager::new();
    TypeModuleBuilder::new(String::from("Animal"))
        .add_method(String::from("die"), Method {})
        .build(&type_modules)
        .unwrap();

    TypeModuleBuilder::new(String::from("Bird"))
        .add_submodule(String::from("Animal"))
        .add_method(String::from("fly"), Method {})
        .build(&type_modules)
        .unwrap();

    TypeModuleBuilder::new(String::from("ExplosiveBird"))
        .add_submodule(String::from("Bird"))
        .add_method(String::from("die"), Method {})
        .build(&type_modules)
        .unwrap();

    //dbg!(&type_modules);
}
